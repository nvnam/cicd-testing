const express = require('express');
const app = express();

app.post('/test', (req, res) => {
    console.log('[Test] -','POST called');
    
    res.status(200).send('post success');
});

app.get('/test', (req, res) => {
    console.log('[Test] -','GET called');
    
    res.status(200).send('get success');
});

var server = app.listen(3000, () => {
    console.log('[Main] -', 'HttpServer Running on', 3000);
});