#!/usr/bin/env bash
# project=$1
# dockerfile=$2
# dir=/mnt/data1/docker_data/_data/video-server/data/${project}/Video

echo This is just an example for deploying and

echo Removing old container...
# docker rm -f ${project}_interface_container

# sh generate_config.sh ${project} ${dockerfile}/config
echo Build image
# docker build ${dockerfile} --tag ${project}_interface:latest --no-cache
echo Run container
# docker run --name ${project}_interface_container \
#         --mount type=bind,source="${dir}",target=/Video \
#         --restart always \
#         --network ${project}_webnet \
# 	-p 0:21509 \
#         -d ${project}_interface:latest
# docker network connect common_net ${project}_interface_container

echo Interface server started


