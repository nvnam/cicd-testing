var assert = require('assert');
var isValidDate = require('./utils/isValidDate').isValidDate;
var getFileSize = require('./utils/file').getFileSize;
var string = require('./utils/string');

// describe('Test get file size', () => {
// 	it('file size', (done) => {
// 		assert.equal(getFileSize("test/testfile.mp4") <= 0, false);
// 		done();
// 	});
// });

describe('Validate datetime for download api', () => {
	it('Success if datetime with format YYYY${spliter}MM${spliter}DD return true and bullshit case will false', (done) => {
		assert.equal(isValidDate('2019-07-12').status, true);
		assert.equal(isValidDate('2019.07.12').status, true);
		assert.equal(isValidDate('2019-27-12').status, false);
		assert.equal(isValidDate('2020-02-29').status, true);
		assert.equal(isValidDate('2100-02-29').status, false);
		assert.equal(isValidDate('2019-01-32').status, false);
		assert.equal(isValidDate('999-07-12').status, false);
		assert.equal(isValidDate('20190712').status, false);
		assert.equal(isValidDate('2019.07-12').status, false);
		assert.equal(isValidDate('Uvuvwevwevwe Onyetenyevwe Ugwemuhwem Osas').status, false);
		assert.equal(isValidDate('').status, false);

		done();
	});
});

describe('string utils work well as expect', () => {
	it('Success base on expectation of string utils', (done) => {
		assert.equal(string.next_range(10, 2), "10-12");
		assert.equal(string.get_extension("abc.txt"), ".txt");
		assert.equal(string.get_extension("nonextension"), "nonextension");
		assert.equal(string.get_th_path("/Interface/test/utils", 1), "Interface");
		assert.equal(string.get_th_path("/Interface/test/utils", 2), "test");
		assert.equal(string.get_th_path("/Interface/test/utils", 3), "utils");
		done();
	});
});