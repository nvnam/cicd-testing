function isValidDate(dateString)
{
    let result = {
        status: false,
        spliter: '',
    };
    // First check for the pattern
    if(!/^\d{4}[./-]\d{2}[./-]\d{2}$/.test(dateString)){
        // console.log('hi mom');
        return result;
    }

    let spliters = ['-', '/', '.'];
    let parts = [];
    // Parse the date parts to integers
    spliters.forEach(element => {
        // console.log(element);
        let split = dateString.split(element);
        // console.log(split);
        if (split.length > 1){
            result.spliter = element;
            parts = split;
        }    
    });

    // console.log(parts.length);
    if (parts.length < 3){
        // console.log('him om');
        return result;
    }

    var day = parseInt(parts[2], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[0], 10);

    // console.log(day, month, year);
    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12){
        // console.log('himom2');
        return result;
    }

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
        monthLength[1] = 29;
    }

    // Check the range of the day
    if (day < 0 || day > monthLength[month - 1]){
        // console.log(monthLength[month - 1]);
        // console.log('himom4');
        return result;
    }

    result.status = true;
    return result;
};

module.exports = {
    isValidDate
}