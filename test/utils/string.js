function get_extension(filename){
    return filename.substring(filename.lastIndexOf('.'), filename.length) || filename;
}

function get_filename(filename){
    return filename.substring(0, filename.lastIndexOf('.')) || filename;
}

function get_first_path(filepath){
    filepath = filepath.replace(/[\\]/g, '/');
    return filepath.substring(0, filepath.indexOf('/') - 1) || filepath;
}

function get_th_path(filepath, order){
    let index = 0;
    let pos = -2;

    while (pos != -1){
        pos = filepath.indexOf('/', pos + 1);
        index = index + 1;

        if (index == order){
            break;
        }
    }

    let end = filepath.indexOf('/', pos + 1);
    if (end === -1){
        end = filepath.length;
    }

    let path = filepath.substring(pos + 1, end);
    console.log(pos, path);
    return path;
}

function get_separate_path(s, separator, n){
    split = s.split(separator);

    if (n < 0 || n > split.length){
        return "";
    }

    return split[n];
}

function next_range(from, range){
    let s = ""
    let to = parseInt(from) + parseInt(range);
    
    if (!isNaN(from) && isFinite(from)){
        s = from + "-" + to;
    }

    return s;
}

module.exports = {
    get_extension,
    get_filename,
    get_first_path,
    get_th_path,
    get_separate_path,
    next_range,
}