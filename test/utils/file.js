const glob = require('glob');
const fs = require('fs');

function find_file_with_level(path, extension, level){
    var globpath = path.substring(0, path.length - level) + "*" + extension;
    return new Promise((resolve) => {
        glob(globpath, {}, (err, files) => {
            if (files.length > 0){
                resolve(files[0]);
            }else{
                resolve(path);
            }
        });
    });
    
}

function getFileSize(path){
    try{
        let stats = fs.statSync(path);
        let fileSizeInBytes = stats.size;
    
        console.log('[FileTest] -', fileSizeInBytes);
        return fileSizeInBytes;
    }
    catch (err) {
        return -1;
    }
}


module.exports = {
    find_file_with_level,
    getFileSize,
}